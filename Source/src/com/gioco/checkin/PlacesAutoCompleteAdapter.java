package com.gioco.checkin;

import java.util.ArrayList;

import com.gioco.checkin.utils.Utils;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
    private ArrayList<String> resultList;
    private long lastTimeMillis;
    private final int DELAY = 1000;
    private int calls = 0;
    
    public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        lastTimeMillis = System.currentTimeMillis();
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

			@Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                	long current = System.currentTimeMillis();
					if (current - lastTimeMillis > DELAY) {
						lastTimeMillis = current;
						Utils.debug("Call: " + ++calls + " made at: " + constraint);
						// Retrieve the autocomplete results.
						resultList = Utils.autocomplete(constraint
								.toString());
					}
                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }
}