package com.gioco.checkin.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.model.GraphUser;
import com.gioco.checkin.bo.User;
import com.gioco.checkin.facebook.LoginActivity;
import com.gioco.checkin.utils.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class ServerRegister extends AsyncTask<Void, Void, Void> {

	private Context context;
	private User user;
	private int statusCode;
	private ProgressDialog progressDialog = null;

	public ServerRegister(Context c, User _user) {
		context = c;
		user = _user;

		progressDialog = new ProgressDialog(context);
		progressDialog.setCancelable(false);
		progressDialog.setIndeterminate(true);
		progressDialog.setMessage("Registering ...");
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		progressDialog.show();
	}

	@Override
	protected Void doInBackground(Void... args) {
		try {

			JSONObject jsonObj = user.toJson();

			JSONArray jsonArray = new JSONArray();
			List<GraphUser> users = LoginActivity.friends;
			if (users != null) {
				for (int i = 0; i < users.size(); i++)
					jsonArray.put(users.get(i).getId());
			}
			
			JSONObject postUser = new JSONObject();
			postUser.put("User", jsonObj);
			postUser.put("fb_ids", jsonArray);

			Log.i("Post user json: ", postUser.toString());
			
			HttpClient httpclient = new DefaultHttpClient();
			HttpConnectionParams.setConnectionTimeout(httpclient.getParams(),
					10000);

			HttpPost httpPost = new HttpPost(Constants.WEB_URL_USER);

			StringEntity entity = new StringEntity(postUser.toString());
			entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));

			httpPost.setEntity(entity);
			httpPost.setHeader("Content-type", "application/json");

			HttpResponse httpResponse = httpclient.execute(httpPost);
			statusCode = httpResponse.getStatusLine().getStatusCode();

			if (statusCode == 201) {

				Log.i("Registering user: ", "Success");
				InputStream inStream = null;
				inStream = httpResponse.getEntity().getContent();
				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inStream));

				while ((line = reader.readLine()) != null) {
					builder.append(line);

				}
				String jsonString = builder.toString();
				try {

					JSONObject userJson = new JSONObject(jsonString);
					User user = User.toUser(userJson);

					user.toLog();

					Utils.serverUser = user;
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	protected void onPostExecute(Void result) {
		progressDialog.dismiss();

		if (statusCode == 201) {

			//Utils.startGettingCheckinNotifications();

			Toast.makeText(context, "User Registered Successfully",
					Toast.LENGTH_LONG).show();
		} else {
			Log.e("Register user failure with status code: ", statusCode + "");
		}
	}
}
