package com.gioco.checkin.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.gioco.checkin.bo.User;
import com.gioco.checkin.utils.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class ServerLogin extends AsyncTask<Void, Void, Void> {

	private int statusCode;

	private ProgressDialog progressDialog = null;
	private Context context;

	private String facebook_id;

	public ServerLogin(Context c, String _facebook_id){
		context = c;
		facebook_id = _facebook_id;
		
		progressDialog = new ProgressDialog(context);
		progressDialog.setCancelable(false);
		progressDialog.setIndeterminate(true);
		progressDialog.setMessage("Logging you in ...");
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		progressDialog.show();
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		HttpResponse response = null;
		HttpGet httpGet;

		try {
			httpGet = new HttpGet(Constants.WEB_URL_USER + "?" +
					Constants.WEB_URL_LOGIN_PARAMETER + "=" + URLEncoder.encode(facebook_id, "UTF-8")
					);

			response = client.execute(httpGet);
			statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				InputStream inStream = null;
				inStream = response.getEntity().getContent();
				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inStream));

				while ((line = reader.readLine()) != null) {
					builder.append(line);

				}
				String jsonString = builder.toString();
				try {
					
					JSONObject userJson = new JSONObject(jsonString);
					User user = User.toUser(userJson);
					
					//checking
					user.toLog();
					
					Utils.serverUser = user;
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			else
				Log.e("Login", "Login failure");
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

		progressDialog.dismiss();

		if(statusCode == 200){
			Toast.makeText(context,"User Signed In", Toast.LENGTH_LONG).show();
//			Utils.startGettingCheckinNotifications();
//			Utils.startGettingMessageNotifications(context);
		}
		else{
			
			String location = "Not Specified";
		    if(Utils.facebookUser.getLocation() != null){
		    	location = (String) Utils.facebookUser.getLocation().getProperty("name");
		    }
		    
		    String gender = "Not Specified";
		    
	    	try {
				gender = (String) Utils.facebookUser.getProperty("gender");
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("SERVER LOGIN", "User's gender not specified");
			}
		    
		    
			User user = new User();
			
			user.setBirthday(Utils.facebookUser.getBirthday())
			.setCreated_At(Utils.getCurrentDateTime())
			.setFacebook_Id(Utils.facebookUser.getId())
			.setGender(gender)
			.setLocation((String) location)
			.setName(Utils.facebookUser.getName())
			.setUsername(Utils.facebookUser.getUsername());
			
			user.toLog();
			
			ServerRegister register = new ServerRegister(context, user);
			register.execute();
		}
	}

}