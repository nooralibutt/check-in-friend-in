package com.gioco.checkin.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import android.os.AsyncTask;
import android.util.Log;

import com.gioco.checkin.utils.Utils;

public class ServerDeleteMessageNotification extends AsyncTask<Void, Void, Void> {

	private int statusCode;

	@Override
	protected Void doInBackground(Void... params) {

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		HttpResponse response = null;
		HttpDelete httpDelete = new HttpDelete(Constants.WEB_URL_MESSAGE_NOTIFICATION
				+ "?" + Constants.WEB_URL_MESSAGE_NOTIFICATION_DELETE_PARAMETER + "="
				+ Utils.serverUser.getId());

		try {
			
			httpDelete.setHeader("Content-type", "application/json");
			response = client.execute(httpDelete);
			statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				InputStream inStream = null;
				inStream = response.getEntity().getContent();
				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inStream));

				while ((line = reader.readLine()) != null) {
					builder.append(line);

				}
				String jsonString = builder.toString();

				Log.i("ServerDeleteMessageNotification", jsonString);
			}
			else
				Log.e("ServerGetMessageNotification",
						"couldn't fetch Message notifications");
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}