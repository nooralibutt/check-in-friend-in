package com.gioco.checkin.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;
import com.gioco.checkin.bo.User;
import com.gioco.checkin.utils.Utils;

public class ServerGetFollowerListCOPY extends AsyncTask<Void, Void, Void> {

	private int statusCode;

	private List<User> userList;
	
	@Override
	protected Void doInBackground(Void... arg0) {
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		HttpResponse response = null;
		HttpGet httpGet = new HttpGet(Constants.WEB_URL_FOLLOWER  + "?" +
				Constants.WEB_URL_FOLLOWER_PARAMETER + "=" + Utils.serverUser.getId());

		try {
			response = client.execute(httpGet);
			statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				InputStream inStream = null;
				inStream = response.getEntity().getContent();
				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inStream));

				while ((line = reader.readLine()) != null) {
					builder.append(line);

				}
				String jsonString = builder.toString();
				
				try {
					
					userList = new ArrayList<User>();
					
					//building array list of notices
					JSONArray usersArray = new JSONArray(jsonString);
					for (int i = 0; i < usersArray.length(); i++) {
						
						JSONObject object = usersArray.getJSONObject(i);
						
						User user = User.toUser(object);
						userList.add(user);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		
		if(statusCode == 200){
			if(userList != null)
				Utils.followsList = userList;
		}
		else
			Log.e("ServerGetFollowers", "couldn't fetch users");
	}
}