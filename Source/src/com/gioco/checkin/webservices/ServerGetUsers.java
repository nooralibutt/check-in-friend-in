package com.gioco.checkin.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.gioco.checkin.UsersListActivity;
import com.gioco.checkin.R;
import com.gioco.checkin.bo.User;
import com.gioco.checkin.utils.Utils;

public class ServerGetUsers extends AsyncTask<String, Void, Void> {

	private int statusCode;

	private ProgressDialog progressDialog = null;

	private UsersListActivity userListActivty;

	private List<User> userList;

	public ServerGetUsers(UsersListActivity _followerActivty){

		userListActivty = _followerActivty;
		
		progressDialog = new ProgressDialog(userListActivty);
		progressDialog.setCancelable(false);
		progressDialog.setIndeterminate(true);
		progressDialog.setMessage("fetching users ...");
	}

	protected void onPreExecute() {
		super.onPreExecute();
		
		progressDialog.show();
	}
	
	@Override
	protected Void doInBackground(String... arg0) {
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		HttpResponse response = null;
		HttpGet httpGet = null;
		try {
			httpGet = new HttpGet(Constants.WEB_URL_USER + "?" +
					Constants.WEB_URL_USER_SEARCH_PARAMETER + "=" + URLEncoder.encode(arg0[0], "UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		try {
			response = client.execute(httpGet);
			statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				InputStream inStream = null;
				inStream = response.getEntity().getContent();
				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inStream));

				while ((line = reader.readLine()) != null) {
					builder.append(line);

				}
				String jsonString = builder.toString();
				
				try {
					
					userList = new ArrayList<User>();
					
					//building array list of notices
					JSONArray usersArray = new JSONArray(jsonString);
					for (int i = 0; i < usersArray.length(); i++) {
						
						JSONObject object = usersArray.getJSONObject(i);
						
						User user = User.toUser(object);
						if(user.getId() != Utils.serverUser.getId() && !isUserMyFollow(user.getId()))
								userList.add(user);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		
		progressDialog.dismiss();
		
		if(statusCode == 200){
			
			//Initializing notice list
			userListActivty.userList = userList;
			
			ArrayAdapter<User> adapter =
					new ArrayAdapter<User>(userListActivty, R.layout.list_item_layout, userList);
			userListActivty.setListAdapter(adapter);
		}
		else
			Log.e("ServerGetUsers", "couldn't fetch users");
	}

	public static boolean isUserMyFollow(int user_id) {
		List<User> followsList = Utils.followsList;
		for (int i = 0; i < followsList.size(); i++) {
			if (user_id == followsList.get(i).getId())
				return true;
		}
		return false;
	}
}