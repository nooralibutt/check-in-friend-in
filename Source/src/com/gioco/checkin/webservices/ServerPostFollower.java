package com.gioco.checkin.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.gioco.checkin.bo.Follower;

public class ServerPostFollower extends AsyncTask<Void, Void, Void>{

	private Context context;
	private int statusCode;
	private ProgressDialog progressDialog = null;
	private Follower follower;
	
	public ServerPostFollower(Context c, Follower _follower) {
		context = c;
		follower = _follower;
		
		progressDialog = new ProgressDialog(context);
		progressDialog.setCancelable(false);
		progressDialog.setIndeterminate(true);
		progressDialog.setMessage("loading ...");
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		progressDialog.show();
	}

	@Override
	protected Void doInBackground(Void... args) {
		try {

			JSONObject jsonObj = follower.toJson();

			HttpClient httpclient = new DefaultHttpClient();
			HttpConnectionParams.setConnectionTimeout(httpclient.getParams(),
					10000);

			HttpPost httpPost = new HttpPost(Constants.WEB_URL_FOLLOWER);

			StringEntity entity = new StringEntity(jsonObj.toString());
			entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));

			httpPost.setEntity(entity);
			httpPost.setHeader("Content-type", "application/json");

			HttpResponse httpResponse = httpclient.execute(httpPost);
			statusCode = httpResponse.getStatusLine().getStatusCode();

			if(statusCode == 201){

				Log.i("Registering Follower: ", "Success");
				InputStream inStream = null;
				inStream = httpResponse.getEntity().getContent();
				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						inStream));
	
				while ((line = reader.readLine()) != null) {
					builder.append(line);
	
				}
				String jsonString = builder.toString();
				try {
					
					JSONObject userJson = new JSONObject(jsonString);
					Follower follower2 = Follower.toFollower(userJson);
					
					follower2.toLog();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	protected void onPostExecute(Void result) {
		progressDialog.dismiss();
		
		if (statusCode == 201) {
			Toast.makeText(context,
					"User Followed Successfully",
					Toast.LENGTH_LONG).show();
		} else {
			Log.e("Register user failure with status code: ", statusCode + "");
		}
	}
}
