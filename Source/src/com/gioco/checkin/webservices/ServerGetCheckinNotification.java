package com.gioco.checkin.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import com.gioco.checkin.bo.CheckInNotification;
import com.gioco.checkin.utils.Utils;

public class ServerGetCheckinNotification {

	private int statusCode;

	private List<CheckInNotification> checkInNotifications;

	public void execute() {
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		HttpResponse response = null;
		HttpGet httpGet = new HttpGet(Constants.WEB_URL_CHECKIN_NOTIFICATION + "?" +
				Constants.WEB_URL_CHECKIN_NOTIFICATION_PARAMETER + "=" + Utils.serverUser.getId());

		try {
			response = client.execute(httpGet);
			statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				InputStream inStream = null;
				inStream = response.getEntity().getContent();
				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inStream));

				while ((line = reader.readLine()) != null) {
					builder.append(line);

				}
				String jsonString = builder.toString();
				
				try {
					
					checkInNotifications = new ArrayList<CheckInNotification>();
					
					//building array list of notices
					JSONArray usersArray = new JSONArray(jsonString);
					for (int i = 0; i < usersArray.length(); i++) {
						
						JSONObject object = usersArray.getJSONObject(i);
						
						CheckInNotification checkInNotification = CheckInNotification.toCheckInNotification(object);
						checkInNotifications.add(checkInNotification);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if(statusCode == 200){
			
			//Initializing notice list
			Utils.checkinNotifications = checkInNotifications;
			Utils.isNotificationListUpdated = true;
			Log.i("ServerGetCheckinNotification", "Notification list updated");
		}
		else
			Log.e("ServerGetCheckinNotification", "couldn't fetch notifications");
	}
}