package com.gioco.checkin.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.gioco.checkin.MapActivity;
import com.gioco.checkin.R;
import com.gioco.checkin.bo.CheckIn;
import com.gioco.checkin.utils.Utils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;

public class ServerPostCheckin extends AsyncTask<Void, Void, Void> {

	private int statusCode;
	private CheckIn checkin;
	private LatLngBounds latLngBounds;
	private GoogleMap mMap;
	private JSONArray checkinJsonArray;
	private Context mContext;

	public ServerPostCheckin(CheckIn _checkin, GoogleMap map, Context c) {
		checkin = _checkin;
		mMap = map;
		mContext = c;
		
		LatLng centre = new LatLng(checkin.getLat(), checkin.getLong());
		latLngBounds = Utils.boundsWithCenterAndLatLngDistance(centre, 1000,
				1000);
	}

	@Override
	protected Void doInBackground(Void... args) {
		try {

			JSONObject jsonObj = checkin.toJson();

			//Log.i("posting json", checkin.getCreated_At());
			//Log.i("posting json", jsonObj.toString());
		
			HttpClient httpclient = new DefaultHttpClient();
			HttpConnectionParams.setConnectionTimeout(httpclient.getParams(),
					10000);

			HttpPost httpPost = new HttpPost(Constants.WEB_URL_CHECKIN);

			StringEntity entity = new StringEntity(jsonObj.toString());
			entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));

			httpPost.setEntity(entity);
			httpPost.setHeader("Content-type", "application/json");

			HttpResponse httpResponse = httpclient.execute(httpPost);
			statusCode = httpResponse.getStatusLine().getStatusCode();

			if (statusCode == 201) {

				InputStream inStream = null;
				inStream = httpResponse.getEntity().getContent();
				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inStream));

				while ((line = reader.readLine()) != null) {
					builder.append(line);

				}
				String jsonString = builder.toString();
				try {

					// 1- traverse each checkin
					// 2- check if it lies within my latlngBounds

					// 2.1- if false check if it is in checkinsAroundMe
					// 2.1.1- if true remove its marker and itself

					// 2.2 if true check if it is in checkinsAroundMe
					// 2.2.1 if true update his checkin
					// 2.2.2 if false add new checkin to googleMap

					checkinJsonArray = new JSONArray(jsonString);
					Log.i("response checkins", checkinJsonArray.toString());

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	protected void onPostExecute(Void result) {

		if (statusCode == 201) {
			Log.i("Checkin Service", "Checkin Posted Successfully");

			for (int i = 0; i < checkinJsonArray.length(); i++) {
				try {
					checkin = CheckIn.toCheckIn(checkinJsonArray.getJSONObject(i));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				// in case of < 0 checkin does not exist
				int indexOfKey = Utils.checkinsAroundMe.indexOfKey(checkin
						.getId());

				int timeDifference = Utils.getTimeDifference(checkin.getCreated_At());
				
				if (latLngBounds.contains(new LatLng(checkin.getLat(), checkin
						.getLong())) && timeDifference < 5) {
					Log.i("Inbound with time difference", checkin.getUser().getName() + " : " + timeDifference);
					
					// if it is in checkinsAroundMe
					if (indexOfKey >= 0) {
						Marker marker = Utils.markersAroundMe.get(checkin
								.getId());
						marker.setPosition(new LatLng(checkin.getLat(), checkin
								.getLong()));
						Utils.checkinsAroundMe.put(checkin.getId(), checkin);
					} else {
						Utils.checkinsAroundMe.put(checkin.getId(), checkin);
						Marker marker = Utils.setMarker(mMap, checkin.getLat(),
								checkin.getLong(), checkin.getId() + "",
								checkin.getText(), null, false);
						Utils.markersAroundMe.put(checkin.getId(), marker);
						
						NotificationCompat.Builder mBuilder =
							    new NotificationCompat.Builder(mContext)
							    .setSmallIcon(R.drawable.ic_launcher)
							    .setContentTitle("Checkin Notification")
							    .setContentText(checkin.getUser().getName());
						
						Intent resultIntent = new Intent(mContext, MapActivity.class);

						// Because clicking the notification opens a new ("special") activity, there's
						// no need to create an artificial back stack.
						PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext, 0,
						    resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
						
						mBuilder.setContentIntent(resultPendingIntent);
						
						// Sets an ID for the notification
						int mNotificationId = 001;
						// Gets an instance of the NotificationManager service
						NotificationManager mNotifyMgr = 
						        (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
						// Builds the notification and issues it.
						mNotifyMgr.notify(mNotificationId, mBuilder.build());
					}
				} else {
					Log.i("outbound with time difference", checkin.getUser().getName() + " : " + timeDifference);
					// if it is in checkinsAroundMe
					if (indexOfKey >= 0) {
						Marker marker = Utils.markersAroundMe.get(checkin
								.getId());
						marker.remove();
						Utils.markersAroundMe.remove(checkin.getId());
						Utils.checkinsAroundMe.removeAt(indexOfKey);
					}
				}
			}
		} else {
			Log.e("checkin posting failure with status code: ", statusCode + "");
		}
	}
}
