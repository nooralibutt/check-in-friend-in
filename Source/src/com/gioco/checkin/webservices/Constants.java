package com.gioco.checkin.webservices;

public class Constants {

	public static final String WEB_URL_FOLLOWER = "http://friendin.apphb.com/api/follower";
	public static final String WEB_URL_FOLLOWER_PARAMETER = "id";

	public static final String WEB_URL_MESSAGE = "http://friendin.apphb.com/api/message";
	public static final String WEB_URL_MESSAGE_PARAMETER = "to";
	public static final String WEB_URL_MESSAGE_NOTIFICATION_DELETE_PARAMETER = "id";

	public static final String WEB_URL_MESSAGE_NOTIFICATION = "http://friendin.apphb.com/api/MessageNotification";
	public static final String WEB_URL_MESSAGE_NOTIFICATION_PARAMETER = "id";

	public static final String WEB_URL_CHECKIN = "http://friendin.apphb.com/api/checkin";

	public static final String WEB_URL_CHECKIN_NOTIFICATION = "http://friendin.apphb.com/api/checkinNotification";
	public static final String WEB_URL_CHECKIN_NOTIFICATION_PARAMETER = "id";


	public static final String WEB_URL_USER = "http://friendin.apphb.com/api/user";
	public static final String WEB_URL_LOGIN_PARAMETER = "facebook_id";
	public static final String WEB_URL_USER_SEARCH_PARAMETER = "id";

}