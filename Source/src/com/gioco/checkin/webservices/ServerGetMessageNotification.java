package com.gioco.checkin.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseArray;

import com.gioco.checkin.bo.CheckIn;
import com.gioco.checkin.bo.MessageNotification;
import com.gioco.checkin.dialog.ReplyMessageDialog;
import com.gioco.checkin.utils.Utils;

public class ServerGetMessageNotification extends AsyncTask<Void, Void, Void> {

	private int statusCode;
	Context context;
	List<MessageNotification> messageNotifications = null;

	public ServerGetMessageNotification(Context c) {
		context = c;
	}

	@Override
	protected Void doInBackground(Void... params) {

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		HttpResponse response = null;
		HttpGet httpGet = new HttpGet(Constants.WEB_URL_MESSAGE_NOTIFICATION
				+ "?" + Constants.WEB_URL_MESSAGE_NOTIFICATION_PARAMETER + "="
				+ Utils.serverUser.getId());

		try {
			response = client.execute(httpGet);
			statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				InputStream inStream = null;
				inStream = response.getEntity().getContent();
				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inStream));

				while ((line = reader.readLine()) != null) {
					builder.append(line);

				}
				String jsonString = builder.toString();

				try {

					messageNotifications = new ArrayList<MessageNotification>();

					// building array list of notices
					JSONArray usersArray = new JSONArray(jsonString);
					for (int i = 0; i < usersArray.length(); i++) {

						JSONObject object = usersArray.getJSONObject(i);

						MessageNotification messageNotification = MessageNotification
								.toMessageNotification(object);
						messageNotifications.add(messageNotification);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		if (statusCode == 200 && messageNotifications != null) {

			// Initializing notice list
			Log.i("ServerGetMessageNotification",
					"Message Notification list updated");

			if (messageNotifications.size() != 0) {
				ServerDeleteMessageNotification serverDeleteMessageNotification = new ServerDeleteMessageNotification();
				serverDeleteMessageNotification.execute();
			}

			for (MessageNotification n : messageNotifications) {
				//int user_Id = n.getUser_Id();
				//if (isUserExist(user_Id)) {
					ReplyMessageDialog replyMessageDialog = new ReplyMessageDialog(
							context, n.getMessage());
					replyMessageDialog.show();
				//}
			}
		} else
			Log.e("ServerGetMessageNotification",
					"couldn't fetch Message notifications");
	}

	public static boolean isUserExist(int user_id) {
		SparseArray<CheckIn> checkins = Utils.checkinsAroundMe;
		for (int i = 0; i < checkins.size(); i++) {
			if (user_id == checkins.valueAt(i).getUser_Id())
				return true;
		}
		return false;
	}
}