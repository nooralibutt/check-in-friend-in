package com.gioco.checkin;

import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;

import com.gioco.checkin.bo.Follower;
import com.gioco.checkin.bo.User;
import com.gioco.checkin.utils.Utils;
import com.gioco.checkin.webservices.ServerGetFollowerListCOPY;
import com.gioco.checkin.webservices.ServerGetUsers;
import com.gioco.checkin.webservices.ServerPostFollower;

public class UsersListActivity extends ListActivity {

	private static final int MENU_FOLLOW_ID = 0;
	public List<User> userList = null;
	private int selectedUerId;
	private EditText etSearchUsers;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_users_list);

		registerForContextMenu(getListView());
		
		ServerGetFollowerListCOPY followerListCOPY = new ServerGetFollowerListCOPY();
		followerListCOPY.execute();
		
		etSearchUsers = (EditText) findViewById(R.id.editTextSearchUsers);
	}
	
	public void searchUsers(View v){
		Utils.hidSoftKeyboard(UsersListActivity.this, v);
		
		String text = etSearchUsers.getText().toString();
		if(text.length() == 0){
			Utils.print(UsersListActivity.this, "Please enter User Name to search");
			return;
		}
		
		ServerGetUsers serverGetUsersList = new ServerGetUsers(this);
		serverGetUsersList.execute(text);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// adding delete dialog when user hold press on any note
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		selectedUerId = (int) info.id;
		menu.add(0, MENU_FOLLOW_ID, 0, "Follow");
	}

	// when user hold press on any note, this method called
	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {

		if (item.getItemId() == MENU_FOLLOW_ID) {
			User user = userList.get(selectedUerId);
			
			Follower follower = new Follower()
					.setCreated_At(Utils.getCurrentDateTime())
					.setFollower_Id(Utils.serverUser.getId())
					.setUser_Id(user.getId());
			
			ServerPostFollower postFollower = new ServerPostFollower(UsersListActivity.this, follower);
			postFollower.execute();
			
			Utils.followsList.add(user);
		}

		return super.onContextItemSelected(item);
	}
}
