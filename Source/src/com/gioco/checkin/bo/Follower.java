package com.gioco.checkin.bo;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Follower {

	private int Id;
	private String Created_At;
	private int User_Id;
	private int Follower_Id;
	private User user;

	public int getId() {
		return Id;
	}

	public String getCreated_At() {
		return Created_At;
	}

	public int getUser_Id() {
		return User_Id;
	}

	public int getFollower_Id() {
		return Follower_Id;
	}

	public User getUser() {
		return user;
	}

	public Follower setUser(User user) {
		this.user = user;
		return this;
	}

	public Follower setId(int id) {
		Id = id;
		return this;
	}

	public Follower setCreated_At(String created_At) {
		Created_At = created_At;
		return this;
	}

	public Follower setUser_Id(int user_Id) {
		User_Id = user_Id;
		return this;
	}

	public Follower setFollower_Id(int follower_Id) {
		Follower_Id = follower_Id;
		return this;
	}

	public void toLog() {
		Log.i("Id", "" + getId());
		Log.i("User_Id", "" + getUser_Id());
		Log.i("Follower_Id", "" + getFollower_Id());
		Log.i("Created_At", getCreated_At());

	}

	public JSONObject toJson() {
		JSONObject jobj = new JSONObject();
		try {

			jobj.put("User_Id", getUser_Id());
			jobj.put("Follower_Id", getFollower_Id());
			jobj.put("Created_At", getCreated_At());

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}

	public static Follower toFollower(JSONObject json) {

		Follower follower = new Follower();

		try {

			follower.setId(json.getInt("Id"))
					.setCreated_At(json.getString("Created_At"))
					.setFollower_Id(json.getInt("User_Id"))
					.setFollower_Id(json.getInt("Follower_Id"));

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		try {
			follower.setUser(User.toUser(json.getJSONObject("User")));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return follower;
	}
}
