package com.gioco.checkin.bo;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class MessageNotification {

	public int getId() {
		return Id;
	}
	public int getFrom() {
		return From;
	}
	public String getCreated_At() {
		return Created_At;
	}
	public int getUser_Id() {
		return User_Id;
	}
	public int getMessage_Id() {
		return Message_Id;
	}
	public Message getMessage() {
		return message;
	}
	public MessageNotification setMessage(Message message) {
		this.message = message;
		return this;
	}
	public MessageNotification setId(int id) {
		Id = id;
		return this;
	}
	public MessageNotification setFrom(int from) {
		From = from;
		return this;
	}
	public MessageNotification setCreated_At(String created_At) {
		Created_At = created_At;
		return this;
	}
	public MessageNotification setUser_Id(int user_Id) {
		User_Id = user_Id;
		return this;
	}
	public MessageNotification setMessage_Id(int message_Id) {
		Message_Id = message_Id;
		return this;
	}
	private int Id ;
    private int From ;
    private String Created_At ;
    private int User_Id ;
    private int Message_Id ;
    private Message message;
    
    public static MessageNotification toMessageNotification(JSONObject json) {

    	MessageNotification msgNot = new MessageNotification();

		try {

			msgNot.setId(json.getInt("Id")).setUser_Id(json.getInt("User_Id")).setMessage_Id(json.getInt("Message_Id"))
					.setFrom(json.getInt("From"))
					.setCreated_At(json.getString("Created_At"));

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}

		try {
			msgNot.setMessage(Message.toMessage(json.getJSONObject("Message")));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return msgNot;
	}
    public JSONObject toJson() {
		JSONObject jobj = new JSONObject();
		try {

			jobj.put("User_Id", getUser_Id());			
			jobj.put("Message_Id", getMessage_Id());
			jobj.put("From", getFrom());
			jobj.put("Created_At", getCreated_At());

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}
    
    public void toLog() {
		Log.i("Id", "" + getId());
		Log.i("Message_Id", "" + getMessage_Id());
		Log.i("From", "" +getFrom());
		Log.i("User_Id","" + getUser_Id());
		Log.i("Created_At","" + getCreated_At());
		
		
	}

	
}
