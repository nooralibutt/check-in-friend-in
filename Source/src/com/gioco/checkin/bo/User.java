package com.gioco.checkin.bo;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class User {

	private int Id;
	private String Name;
	private String Username;
	private String Facebook_Id;
	private String Gender;
	private String Location;
	private String Birthday;
	private String Created_At;

	public int getId() {
		return Id;
	}

	public String getName() {
		return Name;
	}

	public String getUsername() {
		return Username;
	}

	public String getFacebook_Id() {
		return Facebook_Id;
	}

	public String getGender() {
		return Gender;
	}

	public String getLocation() {
		return Location;
	}

	public String getBirthday() {
		return Birthday;
	}

	public String getCreated_At() {
		return Created_At;
	}

	public User setId(int id) {
		Id = id;
		return this;
	}

	public User setName(String name) {
		Name = name;
		return this;
	}

	public User setUsername(String username) {
		Username = username;
		return this;
	}

	public User setFacebook_Id(String facebook_Id) {
		Facebook_Id = facebook_Id;
		return this;
	}

	public User setGender(String gender) {
		Gender = gender;
		return this;
	}

	public User setLocation(String location) {
		Location = location;
		return this;
	}

	public User setBirthday(String birthday) {
		Birthday = birthday;
		return this;
	}

	public User setCreated_At(String created_At) {
		Created_At = created_At;
		return this;
	}

	public static User toUser(JSONObject json) {

		User user = new User();

		try {
			user.setId(json.getInt("Id")).setName(json.getString("Name"))
					.setUsername(json.getString("Username"))
					.setFacebook_Id(json.getString("Facebook_Id"))
					.setGender(json.getString("Gender"))
					.setCreated_At(json.getString("Created_At"))
					.setBirthday(json.getString("Birthday"))
					.setLocation(json.getString("Location"));

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}

		return user;
	}

	public JSONObject toJson() {
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("Username", getUsername());
			jobj.put("Facebook_Id", getFacebook_Id());
			jobj.put("Gender", getGender());
			jobj.put("Name", getName());
			jobj.put("Birthday", getBirthday());
			jobj.put("Created_At", getCreated_At());
			jobj.put("Location", getLocation());
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}

	public void toLog() {
		Log.i("Id", "" + getId());
		Log.i("Username", getUsername());
		Log.i("Facebook_Id", getFacebook_Id());
		Log.i("Gender", "" + getGender());
		Log.i("Name", getName());
		Log.i("Birthday", "" + getBirthday());
		Log.i("Created_At", getCreated_At());
		Log.i("Location", getLocation());
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
