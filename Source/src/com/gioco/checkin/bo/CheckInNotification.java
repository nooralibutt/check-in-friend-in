package com.gioco.checkin.bo;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class CheckInNotification {

	
	public int getId() {
		return Id;
	}
	public String getCreated_At() {
		return Created_At;
	}
	public int getTo() {
		return To;
	}
	public int getFrom() {
		return From;
	}
	public int getCheckIn_Id() {
		return CheckIn_Id;
	}
	public CheckIn getCheckin() {
		return checkin;
	}
	public CheckInNotification setCheckin(CheckIn checkin) {
		this.checkin = checkin;
		return this;
	}
	public CheckInNotification setId(int id) {
		Id = id;
		return this;
	}
	public CheckInNotification setCreated_At(String created_At) {
		Created_At = created_At;
		return this;
	}
	public CheckInNotification setTo(int to) {
		To = to;
		return this;
	}
	public CheckInNotification setFrom(int from) {
		From = from;
		return this;
	}
	public CheckInNotification setCheckIn_Id(int checkIn_Id) {
		CheckIn_Id = checkIn_Id;
		return this;
	}
	
	private int Id ;
    private String Created_At ;
    private int To ;
    private int From ;
    private int CheckIn_Id ;
    private CheckIn checkin;
    
    public static CheckInNotification toCheckInNotification(JSONObject json) {

    	CheckInNotification Checkin = new CheckInNotification();

		try {

			Checkin.setId(json.getInt("Id")).setCheckIn_Id(json.getInt("CheckIn_Id")).setTo(json.getInt("To"))
					.setFrom(json.getInt("From"))
					.setCreated_At(json.getString("Created_At"));

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		try {
			Checkin.setCheckin(CheckIn.toCheckIn(json.getJSONObject("CheckIn")));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return Checkin;
	}

    public JSONObject toJson() {
		JSONObject jobj = new JSONObject();
		try {

			jobj.put("CheckIn_Id", getCheckIn_Id());			
			jobj.put("To", getTo());
			jobj.put("From", getFrom());
			jobj.put("Created_At", getCreated_At());

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}
    
	public void toLog() {
		Log.i("Id", "" + getId());
		Log.i("To", "" + getTo());
		Log.i("From", "" +getFrom());
		Log.i("CheckIn_Id","" + getCheckIn_Id());
		Log.i("Created_At","" + getCreated_At());
		
		
	}

}
