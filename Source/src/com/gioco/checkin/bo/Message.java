package com.gioco.checkin.bo;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Message {

	public int getId() {
		return Id;
	}

	public String getText() {
		return Text;
	}

	public String getCreated_At() {
		return Created_At;
	}

	public int getUser_Id() {
		return User_Id;
	}

	public String getType() {
		return Type;
	}

	public String getUrl() {
		return Url;
	}

	public User getUser() {
		return user;
	}

	public Message setUser(User user) {
		this.user = user;
		return this;
	}

	public Message setId(int id) {
		Id = id;
		return this;
	}

	public Message setText(String text) {
		Text = text;
		return this;
	}

	public Message setCreated_At(String created_At) {
		Created_At = created_At;
		return this;
	}

	public Message setUser_Id(int user_Id) {
		User_Id = user_Id;
		return this;
	}

	public Message setType(String type) {
		Type = type;
		return this;
	}

	public Message setUrl(String url) {
		Url = url;
		return this;
	}

	private int Id;
	private String Text;
	private String Created_At;
	private int User_Id;
	private String Type;
	private String Url;
	private User user;
	
	
	public static Message toMessage(JSONObject json) {

		Message message = new Message();

		try {

			message.setId(json.getInt("Id")).setUser_Id(json.getInt("User_Id"))
					.setCreated_At(json.getString("Created_At"))
					.setText(json.getString("Text"))
					.setType(json.getString("Url"))
					.setType(json.getString("Type"));

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}

		try {
			message.setUser(User.toUser(json.getJSONObject("User")));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return message;
	}

	public JSONObject toJson() {
		JSONObject jobj = new JSONObject();
		try {

			jobj.put("User_Id", getUser_Id());
			jobj.put("Text", getText());
			jobj.put("Url", getUrl());
			jobj.put("Type", getType());
			jobj.put("Created_At", getCreated_At());

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}

	public void toLog() {
		Log.i("Id", "" + getId());
		Log.i("User_Id", "" + getUser_Id());
		Log.i("Text", getText());
		Log.i("Type", getType());
		Log.i("Url", getText());
		Log.i("Created_At", getCreated_At());

	}
}
