package com.gioco.checkin.bo;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class CheckIn {

	private int Id;
	private int User_Id;
	private String Text;
	private double Lat;
	private double Long;
	private String Created_At;
	private User user;
	
	public void toLog() {
		Log.i("Id", "" + getId());
		Log.i("User_Id", "" + getUser_Id());
		Log.i("Text", getText());
		Log.i("Lat","" + getLat());
		Log.i("Long", "" +getLong());		
		Log.i("Created_At", getCreated_At());
		
	}

	public JSONObject toJson() {
		JSONObject jobj = new JSONObject();
		try {

			jobj.put("User_Id", getUser_Id());
			jobj.put("Text", getText());
			jobj.put("Lat", getLat());
			jobj.put("Long", getLong());
			jobj.put("Created_At", getCreated_At());

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}

	public static CheckIn toCheckIn(JSONObject json) {

		CheckIn Checkin = new CheckIn();

		try {

			Checkin.setId(json.getInt("Id")).setUser_Id(json.getInt("User_Id"))
					.setLat(json.getDouble("Lat"))
					.setLong(json.getDouble("Long"))
					.setText(json.getString("Text"))
					.setCreated_At(json.getString("Created_At"));

		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			Checkin.setUser(User.toUser(json.getJSONObject("User")));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return Checkin;
	}

	public int getId() {
		return Id;
	}

	public int getUser_Id() {
		return User_Id;
	}

	public String getText() {
		return Text;
	}

	public double getLat() {
		return Lat;
	}

	public double getLong() {
		return Long;
	}

	public String getCreated_At() {
		return Created_At;
	}

	public User getUser() {
		return user;
	}

	public CheckIn setUser(User user) {
		this.user = user;
		return this;
	}

	public CheckIn setId(int id) {
		Id = id;
		return this;
	}

	public CheckIn setUser_Id(int user_Id) {
		User_Id = user_Id;
		return this;
	}

	public CheckIn setText(String text) {
		Text = text;
		return this;
	}

	public CheckIn setLat(double lat) {
		Lat = lat;
		return this;
	}

	public CheckIn setLong(double l) {
		Long = l;
		return this;
	}

	public CheckIn setCreated_At(String created_At) {
		Created_At = created_At;
		return this;
	}

}
