package com.gioco.checkin.facebook;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import com.facebook.widget.ProfilePictureView;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Request.GraphUserListCallback;
import com.facebook.model.GraphUser;
import com.gioco.checkin.FollowerListActivity;
import com.gioco.checkin.UsersListActivity;
import com.gioco.checkin.MapActivity;
import com.gioco.checkin.R;
import com.gioco.checkin.utils.Utils;
import com.gioco.checkin.webservices.ServerLogin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class LoginActivity extends Activity implements Session.StatusCallback,
		Request.GraphUserCallback, GraphUserListCallback {

	//to get all the friendlist
	//SELECT uid, name, pic_square  FROM user WHERE uid IN 
	//(SELECT uid2 FROM friend WHERE uid1 = me() LIMIT 25)
	
	//to get all the checkins of friends
	//select author_uid, checkin_id, timestamp, coords.longitude, coords.latitude from checkin where author_uid IN 
	//(SELECT uid2 FROM friend WHERE uid1 = me())
	
	public static final String APP_ID = "1433974373512087";
	
	public static List<GraphUser> friends = null;
	public static HashMap<String, String> friendsMap = null;
	
	ProgressDialog progressDialog = null;
	
	private TextView userInfoTextView;
	private ProfilePictureView profilePictureView;
	private Button showFriendsButton;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		progressDialog = new ProgressDialog(LoginActivity.this);
		progressDialog.setCancelable(false);
		progressDialog.setIndeterminate(true);
		progressDialog.setMessage("logging in ...");
		
		userInfoTextView = (TextView) findViewById(R.id.textViewUserInfo);
		profilePictureView = (ProfilePictureView) findViewById(R.id.profile_pic);
		showFriendsButton = (Button) findViewById(R.id.buttonShowFriends);
		showFriendsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(LoginActivity.this, UsersListActivity.class);
                startActivity(i);
			}
		});
		
		Button checkinButton = (Button) findViewById(R.id.buttonCheckin2);
		checkinButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(LoginActivity.this, MapActivity.class));
			}
		});
		
		//generateFacebookHashKey(this);
		
		// start Facebook Login
		Session.openActiveSession(this, true, this);
	}
	
	public static void generateFacebookHashKey(Context c){
		try {
			Log.d("KeyHash:", "generating key hash");
	        PackageInfo info = c.getPackageManager().getPackageInfo(
	                "com.gioco.checkin", 
	                PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
	            }
	    } catch (NameNotFoundException e) {

	    } catch (NoSuchAlgorithmException e) {

	    }
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);
	}

	// callback when session changes state
	@Override
	public void call(Session session, SessionState state, Exception exception) {
		
		if (session.isOpened()) {
			userInfoTextView.setVisibility(View.VISIBLE);
			showFriendsButton.setVisibility(View.VISIBLE);
			
			progressDialog.show();
			
			// make request to the /me API
			Request meRequest = Request.newMeRequest(session, this);
			meRequest.executeAsync();
			
		} else if (state.isClosed()) {
			userInfoTextView.setVisibility(View.INVISIBLE);
			showFriendsButton.setVisibility(View.INVISIBLE);
		}
	}

	//it will be called to get current users information
	@Override
	public void onCompleted(GraphUser user, Response response) {
		
		Utils.facebookUser = user;
		if (user != null) {			
			
			Session session = Session.getActiveSession();

		    if (session != null && session.isOpened()) {
		    	Request myFriendsRequest = Request.newMyFriendsRequest(session, this);
		    	myFriendsRequest.executeAsync();
		    }
		    
			//checking
			//Log.i("user info", user.getInnerJSONObject().toString());
			
            userInfoTextView.setText(buildUserInfoDisplay(user));
            profilePictureView.setProfileId(user.getId());
            showFriendsButton.setVisibility(View.VISIBLE);
		} else {
			showFriendsButton.setVisibility(View.INVISIBLE);
		}
	}
	
	private String buildUserInfoDisplay(GraphUser user) {
	    StringBuilder userInfo = new StringBuilder("");

	    // Example: typed access (name)
	    // - no special permissions required
	    userInfo.append(String.format("Name: %s\n", 
	        user.getName()));

	    // Example: typed access (birthday)
	    // - requires user_birthday permission
	    userInfo.append(String.format("Birthday: %s\n", 
	        user.getBirthday()));
	   
	    String gender = "Not Specified";
	    
    	try {
			gender = (String) user.getProperty("gender");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("SERVER LOGIN", "User's gender not specified");
		}
    	
	    try {
			userInfo.append(String.format("Sex: %s\n", 
					gender));
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    // Example: partially typed access, to location field,
	    // name key (location)
	    // - requires user_location permission
	    String location = "Not Specified";
	    if(user.getLocation() != null){
	    	location = (String) user.getLocation().getProperty("name");
	    }
	    userInfo.append(String.format("Location: %s\n", location ));

	    // Example: access via property name (locale)
	    // - no special permissions required
	    userInfo.append(String.format("Locale: %s\n", 
	        user.getProperty("locale")));
	    
	    return userInfo.toString();
	}

	//getting response from facebook server to get all the friendList
	@Override
	public void onCompleted(final List<GraphUser> users, Response response) {
		progressDialog.dismiss();
		
		ServerLogin serverLogin = new ServerLogin(LoginActivity.this, Utils.facebookUser.getId());
	    serverLogin.execute();
	    
		friends = users;
		
		friendsMap = new HashMap<String, String>();
		
		if(users != null){
			
			for(GraphUser user: users){
				friendsMap.put(user.getId(), user.getName());
			}
			
			//posting friendList to server
			//to get all user friends use this query
			//select uid, name, sex, birthday, username, pic_square, current_location from user where uid in (select uid2 from friend where uid1 = me())
		}
	}

	public void onShowFollowers(View v){
		startActivity(new Intent(LoginActivity.this, FollowerListActivity.class));
	}
}
