package com.gioco.checkin.facebook;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Request.GraphUserListCallback;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

import com.gioco.checkin.R;
import com.gioco.checkin.utils.Utils;

import android.os.Bundle;
import android.os.StrictMode;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("NewApi")
public class FriendListActivity extends Activity {

	Context context;
                                                                                                                                                                                               
	customFriendListAdapter adapter;
	ArrayList<DataModel> dataList = new ArrayList<DataModel>();

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friend_list);

		// if (android.os.Build.VERSION.SDK_INT > 8) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		// }
		
		context = FriendListActivity.this;

		final ListView listViewFirends = (ListView) findViewById(R.id.listViewFriends);
		listViewFirends.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				DataModel model = (DataModel) listViewFirends
						.getItemAtPosition(position);

				showCheckedLocations(model);
			}

		});
		
		Session session = Session.getActiveSession();

	    if (session != null && session.isOpened()) {

			Request.newMyFriendsRequest(session, new GraphUserListCallback() {
				@Override
				public void onCompleted(List<GraphUser> users, Response response) {
					
					if(users == null) return;
					
					String[] names = new String[users.size()];
					String[] id = new String[users.size()];
					
					for (int i = 0; i < users.size(); i++) {
						
						names[i] = users.get(i).getName();
						id[i] = users.get(i).getId();
						
						try {
							String profilePic = id[i];
							dataList.add(new DataModel(names[i], profilePic));
						} catch (Exception e) {
							Log.d("bitmap", " bitmaperror");
							e.printStackTrace();
						}
					}

					adapter = new customFriendListAdapter(context,
							R.layout.row_layout, dataList);

					listViewFirends.setAdapter(adapter);

				}
	    	}).executeAsync();
	    }
	}
	
	protected void showCheckedLocations(DataModel model) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(
				FriendListActivity.this);
		builder.setTitle(model.getName() + "'s Checked Locations");
		
		String fqlQuery = "select checkin_id, timestamp, coords.longitude, coords.latitude " + 
				"from checkin where author_uid ="+ model.getIcon();
		Session session = Session.getActiveSession();
		Bundle params = new Bundle();
		params.putString("q", fqlQuery);
		Request request = new Request(session, "/fql", params, HttpMethod.GET, 
			    new Request.Callback(){ 
			        public void onCompleted(Response response) {
			        ArrayList<String> locations = parseUserFromFQLResponse(response);
			        
			        CharSequence [] items = new CharSequence[locations.size()];
			        items = locations.toArray(items);
			        builder.setItems(items, null);
			        builder.show();
			    }
			});
		Request.executeBatchAsync(request);
	}

	public static ArrayList<String> parseUserFromFQLResponse( Response response )
	{
		ArrayList<String> locations = null;
		try {
			
			GraphObject go = response.getGraphObject();
			JSONObject jso = go.getInnerJSONObject();
			JSONArray arr = jso.getJSONArray("data");
			
			locations = new ArrayList<String>();
			
			for (int i = 0; i < (arr.length()); i++) {
				JSONObject json_obj = arr.getJSONObject(i);

				JSONObject j = json_obj.getJSONObject("coords");
				String longitude = j.getString("longitude");
				String latitude = j.getString("latitude");
				String location = "longitude : " + longitude + "\nlatitude : "
						+ latitude;
				locations.add(location);
			}
		} catch (Exception e) {
			Utils.debug("Excetion while processing checked locations: ", e);
		}
		return locations;
	}
}

class customFriendListAdapter extends ArrayAdapter<DataModel> {

	Context context;
	ArrayList<DataModel> list;

	public customFriendListAdapter(Context context, int textViewResourceId,
			ArrayList<DataModel> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		list = objects;
	}

	@Override
	public int getCount() {

		return list.size();
	};

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View v1 = inflater.inflate(R.layout.row_layout, null);
		ProfilePictureView i1 = (ProfilePictureView) v1.findViewById(R.id.icon);
		TextView name = (TextView) v1.findViewById(R.id.userName);

		name.setText(list.get(position).getName());
		i1.setProfileId(list.get(position).getIcon());
		return v1;
	}

}

class DataModel {
	private String name;
	private String icon;

	public DataModel(String name, String icon) {
		super();
		this.name = name;
		this.icon = icon;
	}

	public DataModel(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}