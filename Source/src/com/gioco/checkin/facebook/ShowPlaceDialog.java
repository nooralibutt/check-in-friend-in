package com.gioco.checkin.facebook;

import java.util.List;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Request.GraphPlaceListCallback;
import com.facebook.model.GraphPlace;
import com.gioco.checkin.bo.CheckIn;
import com.gioco.checkin.utils.Utils;
import com.gioco.checkin.webservices.ServerPostCheckin;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.location.Location;
import android.text.Html;
import android.widget.EditText;

public class ShowPlaceDialog implements GraphPlaceListCallback, OnClickListener {

	private Builder builder;
	private List<GraphPlace> mPlaces;
	private Context mContext;
	private GoogleMap mMap;
	private ProgressDialog mProgressDialog;
	private EditText etComment;
	private GraphPlace graphPlace;
	
	public ShowPlaceDialog(Context context, GoogleMap map) {
		
		mPlaces = null;
		mContext = context;
		mMap = map;
		
		mProgressDialog = new ProgressDialog(
				mContext);
		mProgressDialog
				.setMessage("Loading places ...");
		
		builder = new AlertDialog.Builder(context);
		builder.setTitle("Select Place");		
	}

	public void showPlacesAndCheckin(double lat, double lng, int radius){
		mProgressDialog.show();
		Location location = new Location("kuch bhi");
		location.setLatitude(lat);
		location.setLongitude(lng);
		
		Session session = Session.getActiveSession();
		Request placesSearchRequest = Request.newPlacesSearchRequest(session, location, radius, 
				20, "", this);
		placesSearchRequest.executeAsync();
	}
	
	@Override
	public void onCompleted(List<GraphPlace> places, Response response) {
		
		mPlaces = places;
		if(places != null){
			
			graphPlace = mPlaces.get(0);

			//CheckinUtilities.debug("all the places: " + places.get(0).getInnerJSONObject());
			CharSequence[] placeNames = new CharSequence[places.size()];
			for (int i = 0; i < places.size(); i++) {
				placeNames[i] = Html.fromHtml("<b>" + places.get(i).getName() + 
						"</b>" + 
						" <small>" + places.get(i).getLocation().getStreet() + "</small>");
			}
			
			builder.setSingleChoiceItems(placeNames, 0, this);
			
			
			etComment = new EditText(mContext);
			etComment.setHint("Comment ...");
			builder.setView(etComment);
			builder.setPositiveButton("Checkin", new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String text = etComment.getText().toString();
					
					double lat = graphPlace.getLocation().getLatitude();
					double lng = graphPlace.getLocation().getLongitude();
				
					Utils.getCheckinsfromFacebook(mContext, mMap, new LatLng(lat, lng));
					
					CheckIn checkin = new CheckIn()
					.setCreated_At(Utils.getCurrentDateTime())
					.setLat(lat).setLong(lng).setText(graphPlace.getName() + ":::" + text)
					.setUser_Id(Utils.serverUser.getId());
					
					
					ServerPostCheckin postCheckin = new ServerPostCheckin(checkin, mMap, mContext);
					postCheckin.execute();
					
					//CheckinUtilities.postCheckinToFacebook(graphPlace);
					//Utils.postCheckinToServer("I'm here online", lat, lng);
					Utils.setCustomizedMarker(Utils.facebookUser.getId(), 
							mMap, lat, lng, graphPlace.getName(), graphPlace.getLocation().getStreet());
					Utils.goToLocation(mMap, lat, lng);
				}
			});
//			builder.setItems(placeNames, this);
			mProgressDialog.dismiss();
			builder.show();
		}
		else
			Utils.printLong(mContext, "There are no places at here!");
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		
			graphPlace = mPlaces.get(which);
	}

}
