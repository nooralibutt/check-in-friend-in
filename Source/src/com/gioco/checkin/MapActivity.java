package com.gioco.checkin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gioco.checkin.bo.CheckIn;
import com.gioco.checkin.dialog.NotificationDialog;
import com.gioco.checkin.dialog.SendMessageDialog;
import com.gioco.checkin.facebook.ShowPlaceDialog;
import com.gioco.checkin.utils.Utils;
import com.gioco.checkin.webservices.ServerPostCheckin;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint({ "NewApi", "SimpleDateFormat" })
public class MapActivity extends FragmentActivity implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, LocationListener,
		OnItemClickListener {

	// to hold the error value if google play services are not working
	private static final int GPS_ERRORDIALOG_REQUEST = 9001;

	public static final double PUCIT_LAT = 31.570803;
	public static final double PUCIT_LNG = 74.309503;
	private static final float DEFAULT_ZOOM = 14.0f;

	// it shows interval in which GPS tells you about your current location
	private static final long REQUEST_UPDATE_LOCATION = 20000;

	// to hold the maximum checkin ranges could be placed on map
	private static final int MAX_CHECKIN_RANGES = 5;

	// mMap object holds google map on which all visual elements like markers,
	// circles are drawn
	GoogleMap mMap;

	// to get client's current location programatically
	LocationClient mLocationClient;
	Marker currentLocationMarker;
	private Circle currentRangeCircle;

	// to store all the ranges and bounding box of these circles
	HashMap<String, Circle> ranges;
	HashMap<String, LatLngBounds> bounds;

	private NotificationDialog notificationDialog;

	private String comment = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if google play services are okay!
		if (is_services_ok()) {
			setContentView(R.layout.activity_map);

			// if map has been successfull initialized
			if (initMap()) {

				// mMap.setMyLocationEnabled(true);
				mLocationClient = new LocationClient(this, this, this);

				// creating hashMap to hold ranges and bounds
				ranges = new HashMap<String, Circle>(MAX_CHECKIN_RANGES);
				bounds = new HashMap<String, LatLngBounds>(MAX_CHECKIN_RANGES);

				final AutoCompleteTextView autoCompView = (AutoCompleteTextView) findViewById(R.id.txtLocation);
				autoCompView.setAdapter(new PlacesAutoCompleteAdapter(this,
						R.layout.autocomplete_list_item));
				autoCompView.setOnItemClickListener(this);
				final Drawable x = getResources().getDrawable(
						R.drawable.ic_suggestions_delete);// your x image, this
															// one from standard
															// android images
															// looks pretty good
															// actually
				x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());
				autoCompView.setOnTouchListener(new OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if (autoCompView.getCompoundDrawables()[2] == null) {
							return false;
						}
						if (event.getAction() != MotionEvent.ACTION_UP) {
							return false;
						}
						if (event.getX() > autoCompView.getWidth()
								- autoCompView.getPaddingRight()
								- x.getIntrinsicWidth()) {
							autoCompView.setText("");
							autoCompView.setCompoundDrawables(null, null, null,
									null);
						}
						return false;
					}
				});
				autoCompView.addTextChangedListener(new TextWatcher() {
					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
						autoCompView
								.setCompoundDrawables(null, null, autoCompView
										.getText().toString().equals("") ? null
										: x, null);
					}

					@Override
					public void afterTextChanged(Editable arg0) {
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}
				});

			} else {
				Toast.makeText(this, "Map is not available! :(",
						Toast.LENGTH_SHORT).show();
			}
		}
		// if google play services are not okay then different UI will be shown
		// to user
		else {
			setContentView(R.layout.activity_invalid_map);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean is_services_ok() {
		int isAvailable = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (isAvailable == ConnectionResult.SUCCESS) {
			return true;
		} else if (GooglePlayServicesUtil.isUserRecoverableError(isAvailable)) {
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(isAvailable,
					this, GPS_ERRORDIALOG_REQUEST);
			dialog.show();
		} else {
			Toast.makeText(this, "Can't Connect to Google Play Services",
					Toast.LENGTH_SHORT).show();
		}
		return false;
	}

	private boolean initMap() {
		if (mMap == null) {
			SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map);
			mMap = mapFrag.getMap();

			if (mMap != null) {

				// showing custom window info on marker
				setCustomInfoAdapter();

				// checking in OR drawing circle by pressing long click
				mMap.setOnMapLongClickListener(new OnMapLongClickListener() {

					@Override
					public void onMapLongClick(final LatLng ll) {

						// showing dialog to select from checkin OR placing
						// range
						AlertDialog.Builder builder = new AlertDialog.Builder(
								MapActivity.this);
						builder.setTitle("Pick option").setItems(
								new String[] { "Checkin", "Place range" },
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {

										// if user selected place a range
										if (which == 1) {
											if (ranges.size() < MAX_CHECKIN_RANGES) {

												final SeekBar bar = new SeekBar(
														MapActivity.this);
												bar.setMax(1000);
												AlertDialog.Builder builder = new AlertDialog.Builder(
														MapActivity.this);

												builder.setMessage("0 - 1000")
														.setTitle(
																"Select Range")
														.setPositiveButton(
																"place",
																new DialogInterface.OnClickListener() {
																	public void onClick(
																			DialogInterface dialog,
																			int id) {
																		Circle range = Utils
																				.drawCircle(
																						mMap,
																						ll,
																						bar.getProgress());

																		Marker marker = Utils
																				.setRangeMarker(
																						mMap,
																						ll);
																		ranges.put(
																				marker.getId(),
																				range);
																		bounds.put(
																				marker.getId(),
																				Utils.boundsWithCenterAndLatLngDistance(
																						ll,
																						bar.getProgress() * 2,
																						bar.getProgress() * 2));
																	}
																})
														.setNegativeButton(
																"Cancel",
																new DialogInterface.OnClickListener() {
																	public void onClick(
																			DialogInterface dialog,
																			int id) {
																		dialog.cancel();
																	}
																});

												builder.setView(bar);
												AlertDialog alert = builder
														.create();
												alert.show();

											} else {
												Utils.printLong(
														MapActivity.this,
														"You have already maximum ranges. please delete previous to add another one");
											}
											// if user selected to checkin
										} else if (which == 0) {

											if (!Utils
													.isConnectedOrConnecting(MapActivity.this)) {
												Utils.print(MapActivity.this,
														"Internet is not available.");
												return;
											}

											ShowPlaceDialog placeDialog = new ShowPlaceDialog(
													MapActivity.this, mMap);
											placeDialog.showPlacesAndCheckin(
													ll.latitude, ll.longitude,
													1000);

										}
									}
								});
						builder.show();
					}
				});

				// editing checkin
				mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

					@Override
					public void onInfoWindowClick(final Marker marker) {

						try {
							Integer.parseInt(marker.getTitle());
						} catch (NumberFormatException e) {
							e.printStackTrace();
							return;
						}

						final CheckIn checkin = Utils.checkinsAroundMe
								.get(Integer.parseInt(marker.getTitle()));

						final EditText etComment = new EditText(
								MapActivity.this);

						AlertDialog.Builder builder = new AlertDialog.Builder(
								MapActivity.this);
						builder.setMessage(checkin.getUser().getName())

								.setNeutralButton("Message",
										new DialogInterface.OnClickListener() {
											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												// marker.remove();

												SendMessageDialog messageDialog = new SendMessageDialog(
														MapActivity.this,
														checkin.getUser());
												messageDialog.show();

											}
										})
								.setNegativeButton("Cancel",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												dialog.cancel();
											}
										});

						etComment.setHint("Comment");
						etComment.setText(marker.getSnippet());
						builder.setView(etComment);
						AlertDialog alert = builder.create();
						alert.show();
					}
				});

				// handling dragging of marker
				mMap.setOnMarkerDragListener(new OnMarkerDragListener() {

					@Override
					public void onMarkerDragStart(Marker arg0) {
						Utils.print(MapActivity.this,
								"Place the marker to your desired position");
					}

					@Override
					public void onMarkerDragEnd(final Marker marker) {
						if (!Utils.isConnectedOrConnecting(MapActivity.this)) {
							Utils.print(MapActivity.this,
									"Internet is not available.");
							return;
						}

						new AsyncTask<Double, Void, Void>() {
							String formatedAddress = "";
							ProgressDialog mProgressDialog;

							protected void onPreExecute() {
								mProgressDialog = new ProgressDialog(
										MapActivity.this);
								mProgressDialog.setMessage("Checking in ...");
								mProgressDialog.show();
							};

							@Override
							protected Void doInBackground(Double... bounds) {
								JSONObject json = Utils.getLocationInfo(
										bounds[0], bounds[1]);
								try {
									if (json.getString("status")
											.equalsIgnoreCase("OK")) {
										// here you work with the response JSON
										JSONArray addressList = json
												.getJSONArray("results");
										JSONObject address = addressList
												.getJSONObject(0);
										formatedAddress = address
												.getString("formatted_address");
									} else
										Utils.debug("Status is not OK");
								} catch (JSONException e) {
									e.printStackTrace();
									Utils.debug("json object get kerte howe error: "
											+ e.toString());
								}
								return null;
							}

							protected void onPostExecute(Void result) {
								mProgressDialog.dismiss();
								if (formatedAddress != "") {
									marker.setTitle(formatedAddress);
									marker.showInfoWindow();
								} else
									Utils.print(MapActivity.this,
											"Invalid location OR connection failure");
							};
						}.execute(marker.getPosition().latitude,
								marker.getPosition().longitude);
					}

					@Override
					public void onMarkerDrag(Marker arg0) {
					}
				});

				// to handle ranges
				setOnMarkerClickListener();
			}
		}
		return (mMap != null);
	}

	private void setOnMarkerClickListener() {
		mMap.setOnMarkerClickListener(new OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(final Marker marker) {
				if (ranges.containsKey(marker.getId())) {
					final SeekBar bar = new SeekBar(MapActivity.this);
					bar.setMax(1000);
					final Circle range = ranges.get(marker.getId());
					bar.setProgress((int) range.getRadius());
					AlertDialog.Builder builder = new AlertDialog.Builder(
							MapActivity.this);

					builder.setMessage("Select Range i.e. 0 - 1000")
							.setTitle("Edit Range")
							.setPositiveButton("save",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {

											if (bar.getProgress() != (int) range
													.getRadius()) {
												range.setRadius(bar
														.getProgress());
												bounds.put(
														marker.getId(),
														Utils.boundsWithCenterAndLatLngDistance(
																range.getCenter(),
																bar.getProgress() * 2,
																bar.getProgress() * 2));
											}
										}
									})
							.setNeutralButton("Delete",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											ranges.get(marker.getId()).remove();
											ranges.remove(marker.getId());
											bounds.remove(marker.getId());
											marker.remove();
										}
									})
							.setNegativeButton("Cancel",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											dialog.cancel();
										}
									});

					builder.setView(bar);
					AlertDialog alert = builder.create();
					alert.show();
					return true;
				}
				if(currentLocationMarker != null && currentLocationMarker.getId() == marker.getId())
					return true;
				return false;
			}
		});
	}

	private void setCustomInfoAdapter() {
		mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

			@Override
			public View getInfoWindow(Marker arg0) {
				return null;
			}

			@Override
			public View getInfoContents(Marker marker) {

				View v = getLayoutInflater().inflate(R.layout.info_windows,
						null);

				TextView tvAddress = (TextView) v.findViewById(R.id.tv_address);
				TextView tvComment = (TextView) v.findViewById(R.id.tv_comment);
				TextView tvTime = (TextView) v.findViewById(R.id.tv_time);

				try {
					CheckIn checkin = Utils.checkinsAroundMe.get(Integer
							.parseInt(marker.getTitle()));

					tvAddress.setText(checkin.getUser().getName() + " is here");
					tvComment.setText(checkin.getText());
					tvTime.setText(checkin.getCreated_At());

				} catch (NumberFormatException e) {
					e.printStackTrace();

					tvAddress.setText(marker.getTitle());
					tvComment.setText(marker.getSnippet());
					tvTime.setText(new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss aa").format(new Date()));
				}

				return v;
			}
		});
	}

	public void geoLocate(View v) {

		// hiding Soft Keyboard means when search button pressed keyboard
		// will get down Or hide
		Utils.hidSoftKeyboard(this, v);

		EditText et = (EditText) findViewById(R.id.txtLocation);
		String location = et.getText().toString();
		if (location.length() == 0) {
			Toast.makeText(this, "Please enter a location.", Toast.LENGTH_SHORT)
					.show();
			return;
		}
		if (!Utils.isConnectedOrConnecting(MapActivity.this)) {
			Utils.print(this, "Internet is not available.");
			return;
		}
		new AsyncTask<String, Void, Void>() {
			String formatedAddress = "";
			double latitude;
			double longitude;

			@Override
			protected Void doInBackground(String... locations) {
				JSONObject json = Utils.getLocationInfo(locations[0]);
				Utils.autocomplete(locations[0]);
				try {
					if (json.getString("status").equalsIgnoreCase("OK")) {
						// here you work with the response JSON
						JSONArray addressList = json.getJSONArray("results");
						JSONObject address = addressList.getJSONObject(0);
						formatedAddress = address
								.getString("formatted_address");
						JSONObject location = address.getJSONObject("geometry")
								.getJSONObject("location");
						latitude = location.getDouble("lat");
						longitude = location.getDouble("lng");
					} else
						Utils.debug("Status is not OK");
				} catch (JSONException e) {
					e.printStackTrace();
					Utils.debug("json object get kerte howe error: "
							+ e.toString());
				}
				return null;
			}

			protected void onPostExecute(Void result) {
				if (formatedAddress != "") {
					Utils.goToLocation(mMap, latitude, longitude, DEFAULT_ZOOM);
					Utils.print(MapActivity.this, formatedAddress);
					setMarker(latitude, longitude, formatedAddress,
							"It's a chilling weather :)", null, false);
				} else
					Utils.print(MapActivity.this,
							"Invalid location OR connection failure");
			};
		}.execute(location);
	}

	@Override
	protected void onStart() {
		super.onStart();

		// connecting location client
		mLocationClient.connect();
	}

	@Override
	protected void onStop() {
		super.onStop();

		// saving map state
		MapSateManager mgr = new MapSateManager(this);
		mgr.saveMapState(mMap);
		mgr.saveRanges(ranges);

		// disconnecting location client
		mLocationClient.disconnect();
	}

	@Override
	protected void onResume() {
		super.onResume();

		// loading saved map state
		MapSateManager mgr = new MapSateManager(this);
		CameraPosition position = mgr.getSavedCameraPosition();
		if (position != null) {
			CameraUpdate update = CameraUpdateFactory
					.newCameraPosition(position);
			mMap.moveCamera(update);
		}
		int mapType = mgr.getSavedMapType();
		mMap.setMapType(mapType);
		mgr.getRanges(mMap, ranges, bounds);

		Utils.startGettingMessageNotifications(MapActivity.this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.mapTypeNormal:
			mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			break;
		case R.id.mapTypeSatellite:
			mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			break;
		case R.id.mapTypeTerrain:
			mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
			break;
		case R.id.mapTypeHybrid:
			mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			break;
		case R.id.mapTypeNone:
			mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
			break;
		case R.id.resetMapState:
			clearMapState();
			break;
		case R.id.currentLocation:
			gotoCurrentLocation();
			break;
		case R.id.gps_license:
			Intent intent = new Intent(this, GPSLicenseActivity.class);
			startActivity(intent);
			break;
		case R.id.get_checkin_notifications:
			if (notificationDialog == null || Utils.isNotificationListUpdated)
				notificationDialog = new NotificationDialog(MapActivity.this,
						mMap);
			notificationDialog.show();
			Utils.isNotificationListUpdated = false;
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void gotoCurrentLocation() {
		if (currentLocationMarker != null) {
			Utils.goToLocation(mMap,
					currentLocationMarker.getPosition().latitude,
					currentLocationMarker.getPosition().longitude, DEFAULT_ZOOM);
		}
		// Location loc = mLocationClient.getLastLocation();
		// if (loc == null) {
		// Toast.makeText(this, "Current location isn't available.",
		// Toast.LENGTH_SHORT).show();
		// } else {
		//
		// // placing marker at current position
		// if (currentLocationMarker != null) {
		// currentLocationMarker.remove();
		// }
		//
		// Utils.goToLocation(mMap, loc.getLatitude(), loc.getLongitude(),
		// DEFAULT_ZOOM);
		//
		// // scaling picture to 70 by 70
		// Bitmap original = BitmapFactory.decodeResource(this.getResources(),
		// R.drawable.my_pic);
		// Bitmap b = Bitmap.createScaledBitmap(original, 70, 70, false);
		//
		// currentLocationMarker = setMarker(loc.getLatitude(),
		// loc.getLongitude(), "Current Location",
		// "Home Sweet Home :P",
		// BitmapDescriptorFactory.fromBitmap(b), true);
		// }
	}

	private void clearMapState() {
		MapSateManager mgr = new MapSateManager(MapActivity.this);
		mgr.resetMapState();
		mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		bounds.clear();
		ranges.clear();
		mMap.clear();
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Toast.makeText(this,
				"Connection Failed while getting current location",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onConnected(Bundle arg0) {
		Toast.makeText(this, "Location service is working perfectly fine.",
				Toast.LENGTH_LONG).show();

		LocationRequest request = LocationRequest.create();
		request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		request.setInterval(REQUEST_UPDATE_LOCATION);
		request.setFastestInterval(5000);
		mLocationClient.requestLocationUpdates(request, this);
	}

	@Override
	public void onDisconnected() {
		Toast.makeText(this,
				"Connection Disconnected while getting current location",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onLocationChanged(Location location) {

		LatLng ll = new LatLng(location.getLatitude(), location.getLongitude());
		if(currentRangeCircle != null){
			currentRangeCircle.remove();
			currentRangeCircle = null;
		}
		if(currentLocationMarker != null){
			currentLocationMarker.remove();
			currentLocationMarker = null;
		}
		currentRangeCircle = Utils.drawCircle(mMap, ll, 1000);

		currentLocationMarker = Utils.setRangeMarker(mMap, ll);

		CheckIn checkin = new CheckIn()
				.setCreated_At(Utils.getCurrentDateTime())
				.setLat(location.getLatitude())
				.setLong(location.getLongitude())
				.setUser_Id(Utils.serverUser.getId()).setText(comment);

		ServerPostCheckin postCheckin = new ServerPostCheckin(checkin, mMap, MapActivity.this);
		postCheckin.execute();
	}

	private Marker setMarker(double lat, double lng, String address,
			String comment, BitmapDescriptor icon, boolean isDraggable) {

		LatLng ll = new LatLng(lat, lng);
		// default icon
		if (icon == null)
			icon = BitmapDescriptorFactory
					.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
		MarkerOptions options = new MarkerOptions().title(address).position(ll)
				.snippet(comment).icon(icon).draggable(isDraggable);

		return mMap.addMarker(options);
	}

	// when user clicks on one of the suggestion while searching location
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long id) {
		String str = (String) adapterView.getItemAtPosition(position);
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();

		new AsyncTask<String, Void, JSONObject>() {

			@Override
			protected JSONObject doInBackground(String... params) {
				JSONObject jsonObject = Utils.textSearch(params[0]);
				return jsonObject;
			}

			protected void onPostExecute(JSONObject jsonObject) {
				try {

					Double lat = jsonObject.getJSONObject("geometry")
							.getJSONObject("location").getDouble("lat");
					Double lng = jsonObject.getJSONObject("geometry")
							.getJSONObject("location").getDouble("lng");
					setMarker(lat, lng, jsonObject.getString("name"),
							jsonObject.getString("formatted_address"), null,
							false);
					Utils.goToLocation(mMap, lat, lng);
				} catch (JSONException e) {
					Utils.debug("error in json parsing in item click: ", e);
				}
			}
		}.execute(str);
	}
}
