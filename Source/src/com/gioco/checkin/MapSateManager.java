package com.gioco.checkin;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import com.gioco.checkin.utils.Utils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class MapSateManager {

	private static final String LATITUDE = "latitude";
	private static final String LONGITUDE = "longitude";
	private static final String TILT = "tilt";
	private static final String BEARING = "bearing";
	private static final String MAP_TYPE = "mapType";
	private static final String ZOOM = "zoom";
	private static final String ALL_RANGES = "ranges";
	
	private static final String PREFS_NAME = "mapCameraState";
	
	private SharedPreferences mapStatePrefs;
	
	public MapSateManager(Context context)
	{
		mapStatePrefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
	}
	
	public void saveMapState(GoogleMap map){
		SharedPreferences.Editor editor = mapStatePrefs.edit();
		CameraPosition position = map.getCameraPosition();
		
		editor.putFloat(LATITUDE, (float) position.target.latitude);
		editor.putFloat(LONGITUDE, (float) position.target.longitude);
		editor.putFloat(ZOOM, position.zoom);
		editor.putFloat(TILT, position.tilt);
		editor.putFloat(BEARING, position.bearing);
		editor.putInt(MAP_TYPE, map.getMapType());
		
		editor.commit();
	}
	
	public CameraPosition getSavedCameraPosition(){
		double latitude = mapStatePrefs.getFloat(LATITUDE, 0);
		if (latitude == 0) {
			return null;
		}
		double longitude = mapStatePrefs.getFloat(LONGITUDE, 0);
		LatLng target = new LatLng(latitude, longitude);
		
		float zoom = mapStatePrefs.getFloat(ZOOM, 0);
		float bearing = mapStatePrefs.getFloat(BEARING, 0);
		float tilt = mapStatePrefs.getFloat(TILT, 0);
		
		CameraPosition position = new CameraPosition(target, zoom, tilt, bearing);
		return position;
	}
	
	public int getSavedMapType(){
		return mapStatePrefs.getInt(MAP_TYPE, GoogleMap.MAP_TYPE_NORMAL);
	}

	public boolean saveRanges(HashMap<String, Circle> ranges) {
		SharedPreferences.Editor editor = mapStatePrefs.edit();
		
		Collection<Circle> values = ranges.values();
		Set<String> set= new HashSet<String>();
		
		for (Circle range : values) {
			JSONObject obj = new JSONObject();
			try {
				obj.put("lat", range.getCenter().latitude);
				obj.put("lng", range.getCenter().longitude);
				obj.put("rad", range.getRadius());
			} catch (JSONException e) {
				e.printStackTrace();
				return false;
			}
			set.add(obj.toString());
		}
		
	    editor.putStringSet(ALL_RANGES, set);
	    editor.commit();
		return true;
	}
	
	public boolean getRanges(GoogleMap mMap, HashMap<String,Circle> ranges, HashMap<String,LatLngBounds> bounds)
	{
		Set<String> set = mapStatePrefs.getStringSet(ALL_RANGES, null);
		if(set == null)
			return true;
	    for (String s : set) {
	        try {
	            JSONObject jsonObject = new JSONObject(s);
	            LatLng ll = new LatLng(jsonObject.getDouble("lat"), jsonObject.getDouble("lng"));
	            double rad = jsonObject.getDouble("rad");
	            
	            Circle range = Utils.drawCircle(mMap, ll, rad);
				
				Marker marker = Utils.setRangeMarker(mMap, ll);
				ranges.put(marker.getId(),
						range);
				bounds.put(marker.getId(), 
						Utils.boundsWithCenterAndLatLngDistance(ll, 
								(float)rad * 2, (float)rad * 2));
	        } catch (JSONException e) {
	            e.printStackTrace();
	            return false;
	        }
	    }
	    return true;
	}
	
	public void resetMapState()
	{
		Editor editor = mapStatePrefs.edit();
		editor.clear();
		editor.commit();
	}
}





