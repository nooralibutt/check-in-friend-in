package com.gioco.checkin.dialog;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.EditText;

import com.gioco.checkin.R;
import com.gioco.checkin.bo.Message;
import com.gioco.checkin.utils.Utils;
import com.gioco.checkin.webservices.ServerPostMessage;

public class ReplyMessageDialog implements OnClickListener {

	// private Context mContext;
	private Builder builder;
	private Message senderMessage;
	private EditText etMessage;
	private Context context;

	public ReplyMessageDialog(Context mContext, Message message) {

		this.senderMessage = message;
		this.context = mContext;
		
		builder = new AlertDialog.Builder(context);
		builder.setTitle(message.getUser().getName());
		builder.setIcon(R.drawable.ic_action_social_chat);
		builder.setMessage(message.getText());
		
		etMessage = new EditText(context);
		etMessage.setHint("Reply ...");
		builder.setView(etMessage);
		
		builder.setPositiveButton("Reply", this);
	}

	public void show() {
		builder.show();
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		String text = etMessage.getText().toString();
		if(text.length() == 0)
			Utils.printLong(context, "Please enter some reply to send");
		
		Message message = new Message()
		.setCreated_At(Utils.getCurrentDateTime())
		.setText(text)
		.setType("TEXT")
		.setUrl("Not specified")
		.setUser_Id(Utils.serverUser.getId());
		
		ServerPostMessage postMessage = new ServerPostMessage(context, message, senderMessage.getUser().getId());
		postMessage.execute();	
	}

}
