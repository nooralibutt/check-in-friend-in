package com.gioco.checkin.dialog;

import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.Html;

import com.gioco.checkin.R;
import com.gioco.checkin.bo.CheckIn;
import com.gioco.checkin.bo.CheckInNotification;
import com.gioco.checkin.bo.User;
import com.gioco.checkin.utils.Utils;
import com.google.android.gms.maps.GoogleMap;

public class NotificationDialog implements OnClickListener {

	private GoogleMap mMap;
//	private Context mContext;
	private Builder builder;
	private List<CheckInNotification> notificationList;

	public NotificationDialog(Context context, GoogleMap map) {
		
		mMap = map;
//		mContext = context;
		
		builder = new AlertDialog.Builder(context);
		builder.setTitle("Checkin Notifications");	
		builder.setIcon(R.drawable.action_location);
		
		notificationList = Utils.checkinNotifications;
		
		CharSequence[] notifications = null;
		if (notificationList == null || notificationList.size() == 0) {
			notifications = new CharSequence[1];
			notifications[0] = Html.fromHtml("<i>No notifications</i>");
		} else {
			notifications = new CharSequence[notificationList.size()];
			for (int i = 0; i < notificationList.size(); i++) {

				CheckInNotification checkInNotification = notificationList
						.get(i);
				User user = checkInNotification.getCheckin().getUser();
				notifications[i] = Html.fromHtml("<b>" + user.getName()
						+ "</b>");
			}
		}
		builder.setItems(notifications, this);
	}

	public void show(){
		builder.show();
	}
	
	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (notificationList == null || notificationList.size() == 0)
			return;
		CheckInNotification checkInNotification = notificationList.get(which);
		CheckIn checkin = checkInNotification.getCheckin();

		String text = checkin.getText();
		
		String address = "Not specified";
		String comment = text;
		
		int indexOf = text.indexOf(":::");
		if(indexOf != -1){
			address = text.substring(0, indexOf);
			comment = text.substring(indexOf + 3);
		}
		
		Utils.setCustomizedMarker(checkin.getUser().getFacebook_Id(), mMap, 
				checkin.getLat(), checkin.getLong(), address, comment);
		
		Utils.goToLocation(mMap, checkin.getLat(), checkin.getLong());
	}

}
