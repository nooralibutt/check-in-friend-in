package com.gioco.checkin.dialog;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.EditText;

import com.gioco.checkin.R;
import com.gioco.checkin.bo.Message;
import com.gioco.checkin.bo.User;
import com.gioco.checkin.utils.Utils;
import com.gioco.checkin.webservices.ServerPostMessage;

public class SendMessageDialog implements OnClickListener {

	// private Context mContext;
	private Builder builder;
	private User user;
	private EditText etMessage;
	private Context context;

	public SendMessageDialog(Context mContext, User user) {

		this.user = user;
		this.context = mContext;
		
		builder = new AlertDialog.Builder(context);
		builder.setTitle(user.getName());
		builder.setIcon(R.drawable.ic_action_social_chat);

		etMessage = new EditText(context);
		etMessage.setHint("Message ...");
		builder.setView(etMessage);
		
		builder.setPositiveButton("Send", this);
	}

	public void show() {
		builder.show();
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		String text = etMessage.getText().toString();
		if(text.length() == 0)
			Utils.printLong(context, "Please enter some message to send");
		
		Message message = new Message()
		.setCreated_At(Utils.getCurrentDateTime())
		.setText(text)
		.setType("TEXT")
		.setUrl("Not specified")
		.setUser_Id(Utils.serverUser.getId());
		
		ServerPostMessage postMessage = new ServerPostMessage(context, message, user.getId());
		postMessage.execute();	
	}

}
