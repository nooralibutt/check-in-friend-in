package com.gioco.checkin;

import com.google.android.gms.common.GooglePlayServicesUtil;

import android.os.Bundle;
import android.app.Activity;
import android.view.MenuItem;
import android.widget.TextView;

public class GPSLicenseActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gps_license);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		// display license terms here
		String license = GooglePlayServicesUtil
				.getOpenSourceSoftwareLicenseInfo(this);
		TextView tv = (TextView) findViewById(R.id.gps_license_text);

		if (license != null) {
			tv.setText(license);
		} else {
			tv.setText("Google Play services are not installed on this device.");
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

}
