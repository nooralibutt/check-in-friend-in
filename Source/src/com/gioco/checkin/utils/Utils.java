package com.gioco.checkin.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Request.Callback;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphPlace;
import com.facebook.model.GraphUser;
import com.gioco.checkin.R;
import com.gioco.checkin.bo.CheckIn;
import com.gioco.checkin.bo.CheckInNotification;
import com.gioco.checkin.bo.MessageNotification;
import com.gioco.checkin.bo.User;
import com.gioco.checkin.facebook.LoginActivity;
import com.gioco.checkin.webservices.ServerGetCheckinNotification;
import com.gioco.checkin.webservices.ServerGetMessageNotification;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

//This class contains Utility functions
public class Utils {

	public static final String LOG_TAG = "CHECKIN";
	public static final String API_KEY = "AIzaSyADOsX0Bm3hKw9VfrnJ-sDLMVrLTReOQHE";

	public static User serverUser = null;
	public static GraphUser facebookUser = null;

	public static List<User> followsList = new ArrayList<User>();
	public static List<CheckInNotification> checkinNotifications = null;
	public static List<MessageNotification> messageNotifications = null;

	public static SparseArray<Marker> markersAroundMe = new SparseArray<Marker>();
	public static SparseArray<CheckIn> checkinsAroundMe = new SparseArray<CheckIn>();

	public static boolean isNotificationListUpdated = false;

	// hiding Soft Keyboard means when search button pressed keyboard will get
	// down Or hide
	public static void hidSoftKeyboard(Context context, View v) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	public static void print(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	public static void printLong(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
	}

	public static void debug(String msg) {
		Log.d(LOG_TAG, msg);
	}

	public static void debug(String msg, Exception e) {
		Log.d(LOG_TAG, msg + ": " + e.toString());
	}

	public static String getCurrentDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		Date date = new Date();

		return dateFormat.format(date);
	}

	public static Date getDate(String date) {
		date = date.replace('T', ' ');
		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:SS", Locale.getDefault());
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int getTimeDifference(String date) {
		Date userDate = getDate(date);
		if (userDate != null) {
			Date currentDateTime = new Date();
			return (int) ((currentDateTime.getTime() / 60000) - (userDate
					.getTime() / 60000));
		}
		return 1000;
	}

	public static void goToLocation(GoogleMap mMap, double lat, double lng) {
		LatLng ll = new LatLng(lat, lng);
		CameraUpdate update = CameraUpdateFactory.newLatLng(ll);
		mMap.animateCamera(update);
	}

	public static void goToLocation(GoogleMap mMap, double lat, double lng,
			float zoom) {
		LatLng ll = new LatLng(lat, lng);
		CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
		mMap.animateCamera(update);
	}

	public static Marker setRangeMarker(GoogleMap mMap, LatLng ll) {
		MarkerOptions options = new MarkerOptions()
				.position(ll)
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.ic_mapmarker))
				.anchor(0.5f, 0.5f);
		return mMap.addMarker(options);
	}

	public static Marker setMarker(GoogleMap mMap, double lat, double lng,
			String address, String comment, BitmapDescriptor icon,
			boolean isDraggable) {

		LatLng ll = new LatLng(lat, lng);
		// default icon
		if (icon == null)
			icon = BitmapDescriptorFactory
					.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
		MarkerOptions options = new MarkerOptions().title(address).position(ll)
				.snippet(comment).icon(icon).draggable(isDraggable);

		return mMap.addMarker(options);
	}

	public static void startGettingCheckinNotifications() {
		TimerTask timerTask = new TimerTask() {

			@Override
			public void run() {
				ServerGetCheckinNotification getCheckinNotification = new ServerGetCheckinNotification();
				getCheckinNotification.execute();
			}
		};
		new Timer().schedule(timerTask, 5000, 30000);
	}

	public static void startGettingMessageNotificationsHandler(final Context c) {
		TimerTask timerTask = new TimerTask() {

			@Override
			public void run() {
				ServerGetMessageNotification getCheckinNotification = new ServerGetMessageNotification(
						c);
				getCheckinNotification.execute();
			}
		};
		new Timer().schedule(timerTask, 5000, 10000);
	}

	public static void startGettingMessageNotifications(final Context c) {
		final Handler m_handler = new Handler();
		;
		Runnable m_handlerTask = null;
		m_handlerTask = new Runnable() {
			@Override
			public void run() {
				m_handler.postDelayed(this, 10000);
				ServerGetMessageNotification getCheckinNotification = new ServerGetMessageNotification(
						c);
				getCheckinNotification.execute();
			}
		};
		m_handlerTask.run();
	}

	public static void setCustomizedMarker(final String userID,
			final GoogleMap mMap, final double lat, final double lng,
			final String address, final String comment) {
		new AsyncTask<Void, Void, Void>() {
			Bitmap bitmap = null;

			@Override
			protected Void doInBackground(Void... params) {
				try {
					String imageURL = "https://graph.facebook.com/" + userID
							+ "/picture?type=small";
					URL url = new URL(imageURL);
					bitmap = BitmapFactory.decodeStream(url.openConnection()
							.getInputStream());
					if (bitmap == null)
						Utils.debug("FAILED Loading Picture");

				} catch (Exception e) {
					Utils.debug("Loading Picture FAILED", e);
					e.printStackTrace();
				}
				return null;
			}

			protected void onPostExecute(Void result) {

				BitmapDescriptor bitmapDescriptor = null;
				if (bitmap != null) {
					bitmap = Bitmap.createScaledBitmap(bitmap, 60, 60, false);
					bitmapDescriptor = BitmapDescriptorFactory
							.fromBitmap(bitmap);
				}
				setMarker(mMap, lat, lng, address, comment, bitmapDescriptor,
						false);
			};
		}.execute();
	}

	public static Circle drawCircle(GoogleMap mMap, LatLng ll, double radius) {
		CircleOptions options = new CircleOptions().center(ll)
				.fillColor(0x330000FF).strokeColor(Color.BLUE).strokeWidth(3)
				.radius(radius);

		return mMap.addCircle(options);
	}

	public static JSONObject getLocationInfo(String address) {

		address = address.replace(" ", "+");
		// HttpGet httpGet = new
		// HttpGet("https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=false");
		HttpGet httpGet = new HttpGet(
				"https://maps.googleapis.com/maps/api/geocode/json?address="
						+ address + "&sensor=true");
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			debug("getLoginInfo: " + e.toString());
		} catch (IOException e) {
			e.printStackTrace();
			debug("getLoginInfo: " + e.toString());
		}
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {
			e.printStackTrace();
			debug("getLoginInfo main json object banate howe error: "
					+ e.toString());
		}

		return jsonObject;
	}

	public static JSONObject getLocationInfo(double lat, double lng) {

		// HttpGet httpGet = new
		// HttpGet("https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=false");
		HttpGet httpGet = new HttpGet(
				"https://maps.googleapis.com/maps/api/geocode/json?latlng="
						+ lat + "," + lng + "&sensor=true");
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			debug("getLoginInfo: " + e.toString());
		} catch (IOException e) {
			e.printStackTrace();
			debug("getLoginInfo: " + e.toString());
		}
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {
			e.printStackTrace();
			debug("getLoginInfo main json object banate howe error: "
					+ e.toString());
		}

		return jsonObject;
	}

	public static JSONArray getPlacesFromGoogle(double lat, double lng,
			int radius) {

		JSONArray jsonArray = null;

		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();
		try {
			StringBuilder sb = new StringBuilder(
					"https://maps.googleapis.com/maps/api/place/nearbysearch/json");
			sb.append("?location=" + lat + "," + lng + "&radius=" + radius);
			sb.append("&types="
					+ URLEncoder.encode(
							"restaurant|bakery|movie_theater|cafe|bar", "utf8"));
			sb.append("&sensor=false&key=" + API_KEY);

			URL url = new URL(sb.toString());
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());

			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			debug("Error processing PlaceInfo API URL", e);
			return jsonArray;
		} catch (IOException e) {
			debug("Error connecting to PlaceInfo API", e);
			return jsonArray;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			jsonArray = jsonObj.getJSONArray("results");

		} catch (JSONException e) {
			debug("Cannot process JSON results", e);
		}

		return jsonArray;
	}

	public static ArrayList<String> autocomplete(String input) {
		ArrayList<String> resultList = null;

		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();
		try {
			StringBuilder sb = new StringBuilder(
					"https://maps.googleapis.com/maps/api/place/autocomplete/json");
			sb.append("?sensor=false&key=" + API_KEY);
			// sb.append("&components=country:pk");
			sb.append("&input=" + URLEncoder.encode(input, "utf8"));

			URL url = new URL(sb.toString());
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());

			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			debug("Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			debug("Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<String>(predsJsonArray.length());
			for (int i = 0; i < predsJsonArray.length(); i++) {
				resultList.add(predsJsonArray.getJSONObject(i).getString(
						"description"));
			}
		} catch (JSONException e) {
			debug("Cannot process JSON results", e);
		}

		return resultList;
	}

	public static JSONObject textSearch(String query) {
		JSONObject jsonObject = null;

		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();
		try {
			StringBuilder sb = new StringBuilder(
					"https://maps.googleapis.com/maps/api/place/textsearch/json");
			sb.append("?query=" + URLEncoder.encode(query, "utf8"));
			sb.append("&sensor=false&key=" + API_KEY);

			URL url = new URL(sb.toString());
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());

			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			debug("Error processing TextSearch API URL", e);
			return jsonObject;
		} catch (IOException e) {
			debug("Error connecting to TextSearch API", e);
			return jsonObject;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("results");

			debug(predsJsonArray.getJSONObject(0).getString("name"));
			jsonObject = predsJsonArray.getJSONObject(0);
		} catch (JSONException e) {
			debug("Cannot process JSON results", e);
		}

		return jsonObject;
	}

	public static void postCheckinToFacebook(GraphPlace graphPlace) {
		final Session session = Session.getActiveSession();
		Request statusUpdateRequest = Request.newStatusUpdateRequest(session,
				"Hell Yea :P #fyp #success #viaCheckinApp", graphPlace, null,
				new Callback() {

					@Override
					public void onCompleted(Response response) {
						debug("response of posting checkin to facebook: "
								+ response.toString());
					}
				});
		statusUpdateRequest.executeAsync();
	}

	public static void getCheckinsfromFacebook(final Context context,
			final GoogleMap mMap, final LatLng ll) {

		String fqlQuery = "select author_uid, checkin_id, message, timestamp, coords.longitude, coords.latitude "
				+ "from checkin where author_uid IN "
				+ "(SELECT uid2 FROM friend WHERE uid1 = me())";

		Session session = Session.getActiveSession();

		Bundle params = new Bundle();
		params.putString("q", fqlQuery);

		new Request(session, "/fql", params, HttpMethod.GET,
				new Request.Callback() {
					public void onCompleted(Response response) {

						if (response == null) {
							Log.e("getCheckinsfromFacebook", "response == null");
							return;
						}
						try {
							// CheckinUtilities.debug(response.getGraphObject().getInnerJSONObject().getJSONArray("data").toString(2));

							JSONArray jsonArray = response.getGraphObject()
									.getInnerJSONObject().getJSONArray("data");

							LatLngBounds latLngBounds = boundsWithCenterAndLatLngDistance(
									ll, 1000, 1000);

							for (int i = 0; i < jsonArray.length(); i++) {
								JSONObject coords = jsonArray.getJSONObject(i)
										.getJSONObject("coords");
								if (latLngBounds.contains(new LatLng(coords
										.getDouble("latitude"), coords
										.getDouble("longitude")))) {

									Double lat = coords.getDouble("latitude");
									Double lng = coords.getDouble("longitude");
									String message = jsonArray.getJSONObject(i)
											.getString("message");
									String id = jsonArray.getJSONObject(i)
											.getString("author_uid");
									String address = LoginActivity.friendsMap
											.get(id) + " was here";

									Utils.setCustomizedMarker(id, mMap, lat,
											lng, address, message);
								}
							}
						} catch (JSONException e) {
							e.printStackTrace();
							Utils.debug(
									"Error processing resopnse from facebook at loading checkins",
									e);
						}
					}
				}).executeAsync();
	}

	public static boolean isConnectedOrConnecting(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public boolean isConnectingToInternet(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}

		}
		return false;
	}

	private static final double ASSUMED_INIT_LATLNG_DIFF = 1.0;
	private static final float ACCURACY = 0.01f;

	public static LatLngBounds boundsWithCenterAndLatLngDistance(LatLng center,
			float latDistanceInMeters, float lngDistanceInMeters) {
		latDistanceInMeters /= 2;
		lngDistanceInMeters /= 2;
		LatLngBounds.Builder builder = LatLngBounds.builder();
		float[] distance = new float[1];
		{
			boolean foundMax = false;
			double foundMinLngDiff = 0;
			double assumedLngDiff = ASSUMED_INIT_LATLNG_DIFF;
			do {
				Location.distanceBetween(center.latitude, center.longitude,
						center.latitude, center.longitude + assumedLngDiff,
						distance);
				float distanceDiff = distance[0] - lngDistanceInMeters;
				if (distanceDiff < 0) {
					if (!foundMax) {
						foundMinLngDiff = assumedLngDiff;
						assumedLngDiff *= 2;
					} else {
						double tmp = assumedLngDiff;
						assumedLngDiff += (assumedLngDiff - foundMinLngDiff) / 2;
						foundMinLngDiff = tmp;
					}
				} else {
					assumedLngDiff -= (assumedLngDiff - foundMinLngDiff) / 2;
					foundMax = true;
				}
			} while (Math.abs(distance[0] - lngDistanceInMeters) > lngDistanceInMeters
					* ACCURACY);
			LatLng east = new LatLng(center.latitude, center.longitude
					+ assumedLngDiff);
			builder.include(east);
			LatLng west = new LatLng(center.latitude, center.longitude
					- assumedLngDiff);
			builder.include(west);
		}
		{
			boolean foundMax = false;
			double foundMinLatDiff = 0;
			double assumedLatDiffNorth = ASSUMED_INIT_LATLNG_DIFF;
			do {
				Location.distanceBetween(center.latitude, center.longitude,
						center.latitude + assumedLatDiffNorth,
						center.longitude, distance);
				float distanceDiff = distance[0] - latDistanceInMeters;
				if (distanceDiff < 0) {
					if (!foundMax) {
						foundMinLatDiff = assumedLatDiffNorth;
						assumedLatDiffNorth *= 2;
					} else {
						double tmp = assumedLatDiffNorth;
						assumedLatDiffNorth += (assumedLatDiffNorth - foundMinLatDiff) / 2;
						foundMinLatDiff = tmp;
					}
				} else {
					assumedLatDiffNorth -= (assumedLatDiffNorth - foundMinLatDiff) / 2;
					foundMax = true;
				}
			} while (Math.abs(distance[0] - latDistanceInMeters) > latDistanceInMeters
					* ACCURACY);
			LatLng north = new LatLng(center.latitude + assumedLatDiffNorth,
					center.longitude);
			builder.include(north);
		}
		{
			boolean foundMax = false;
			double foundMinLatDiff = 0;
			double assumedLatDiffSouth = ASSUMED_INIT_LATLNG_DIFF;
			do {
				Location.distanceBetween(center.latitude, center.longitude,
						center.latitude - assumedLatDiffSouth,
						center.longitude, distance);
				float distanceDiff = distance[0] - latDistanceInMeters;
				if (distanceDiff < 0) {
					if (!foundMax) {
						foundMinLatDiff = assumedLatDiffSouth;
						assumedLatDiffSouth *= 2;
					} else {
						double tmp = assumedLatDiffSouth;
						assumedLatDiffSouth += (assumedLatDiffSouth - foundMinLatDiff) / 2;
						foundMinLatDiff = tmp;
					}
				} else {
					assumedLatDiffSouth -= (assumedLatDiffSouth - foundMinLatDiff) / 2;
					foundMax = true;
				}
			} while (Math.abs(distance[0] - latDistanceInMeters) > latDistanceInMeters
					* ACCURACY);
			LatLng south = new LatLng(center.latitude - assumedLatDiffSouth,
					center.longitude);
			builder.include(south);
		}
		return builder.build();
	}
}