package com.gioco.checkin;

import java.util.List;

import com.gioco.checkin.bo.User;
import com.gioco.checkin.dialog.SendMessageDialog;
import com.gioco.checkin.webservices.ServerGetFollowerList;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class FollowerListActivity extends ListActivity {

	private static final int MENU_SEND_MESSAGE_ID = 0;
	private static final int MENU_UNFOLLOW_ID = 1;

	public List<User> userList = null;
	private int selectedUerId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_follower_list);

		refreshDisplay();
		registerForContextMenu(getListView());
	}

	private void refreshDisplay() {
		ServerGetFollowerList serverGetFollowerList = new ServerGetFollowerList(
				this);
		serverGetFollowerList.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// adding delete dialog when user hold press on any note
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		selectedUerId = (int) info.id;
		menu.add(0, MENU_SEND_MESSAGE_ID, 0, "Send Message");
		menu.add(0, MENU_UNFOLLOW_ID, 0, "UnFollow");
	}

	// when user hold press on any note, this method called
	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {

		if (item.getItemId() == MENU_SEND_MESSAGE_ID) {
			User user = userList.get(selectedUerId);

			SendMessageDialog messageDialog = new SendMessageDialog(
					FollowerListActivity.this, user);
			messageDialog.show();
			
		} else if (item.getItemId() == MENU_UNFOLLOW_ID) {

		}

		return super.onContextItemSelected(item);
	}
}
